'use strict';
/**
 * not overly happy about the code here, however the test already had 100% completion still when
 * I rewrote the index.js. This was one of the first unit tests I wrote 2 years ago in node
 */

var assert = require('assert');
var rewire = require('rewire');

var token = rewire('../index.js');
// override the console.log we don't want that showing up in unit tests
token.__set__('console', { 'log': () => { } });

describe('NilFactor MultiToken', () => {
    var response = () => { };
    var request = {
        url: '/',
        headers: {'Content-type': 'application/json'},
    };

    describe('Requires authentication key', () => {
        var resp = false;
        response.send = function(status, message) {
            resp = true;
            it('Status is 401', () => {
                assert.equal(status, 401);
            });
            it('Response is as expected', () => {
                assert.equal(typeof message, "object");
                assert.equal(message.code, 'UnauthorizedRequest');
                assert.equal(message.result[0].error, 'Request Denied');
            });
        };
        token.verifyRequest(request, response);

        it('Sent a Response', () => {
            assert.equal(resp, true);
        });
    });

    // added here to allow for testing branch on line 74 in index.js
    token.addIgnoreRoute('/test');
    token.addIgnoreRoute('/test2/*');

    describe('Requires authentication key to match expected and rejects', () => {
        var resp = false;
        request.headers['key'] = 'wrong-password';
        request.connection = {remoteAddress: '::ffff:127.0.0.1'};
        response.send = (status, message) => {
            resp = true;
            it('Status is 401', () => {
                assert.equal(status, 401);
            });
            it('Response is as expected and rejected', () => {
                assert.equal(typeof message, "object");
                assert.equal(message.code, 'UnauthorizedRequest');
                assert.equal(message.result[0].error, 'Request Denied');
            });
        };
        token.verifyRequest(request, response);

        it('Sent a Response', () => {
            assert.equal(resp, true);
        });
    });

    describe('Requires authentication key to match expected and token matches', () => {
        var resp = false;
        var next = false;
        request.headers['key'] = 'token1';

        response.send = (status, message) => {
            resp = true;
        };

        token.verifyRequest(request, response, () => {
            next = true;
        });

        it('Did not send a Response as token matches', () => {
            assert.equal(resp, false);
        });

        it('Did run next', () => {
            assert.equal(next, true);
        });

    });

    describe('Allows for ignored routes to be added via code', () => {
        it('Added route succesfully', () => {
            var routes = token.getIgnoredRoutes();
            assert.equal('/test', routes[0]);
            assert.equal('/test2/*', routes[1]);
        });
    });

    describe('Allows routes through as expected without token [route: /test]', () => {
        var resp = false;
        var next = false;

        var req = {
            headers: [],
            url: '/test'
        };

        response.send = (request, message) => {
            resp = true;
        };
        token.verifyRequest(req, response, () => {
            next = true;
        });
        it('Did not send a Response', () => {
            assert.equal(resp, false);
        });
        it('Did run next', () => {
            assert.equal(next, true);
        });
    });

    describe('Allows routes through as expected without token [route: /test2/baby]', () => {
        var resp = false;
        var next = false;

        var req = {
            headers: [],
            url: '/test2/baby'
        };

        response.send = (request, message) => {
            resp = true;
        };
        token.verifyRequest(req, response, () => {
            next = true;
        });
        it('Did not send a Response', () => {
            assert.equal(resp, false);
        });
        it('Did run next', () => {
            assert.equal(next, true);
        });
    });

    describe('Allows alternate apiUser token: test_one', () => {
        var resp = false;
        var next = false;

        var req = {
            headers: {key: "token2"},
            url: '/test/moo'
        };

        response.send = (request, message) => {
            resp = true;
        };
        token.verifyRequest(req, response, () => {
            next = true;
        });

        it('Did not send a Response', () => {
            assert.equal(resp, false);
        });

        it('Did run next', () => {
            assert.equal(next, true);
        });
    });

    describe('Denies alternate apiUser token: test_one, based on ruleset', () => {
        var resp = false;
        var next = false;

        var req = {
            headers: {key: "token2"},
            url: '/fail/moo'
        };

        response.send = (request, message) => {
            resp = true;
        };
        token.verifyRequest(req, response, () => {
            next = true;
        });

        it('Did send a Response', () => {
            assert.equal(resp, true);
        });

        it('Did not run next', () => {
            assert.equal(next, false);
        });
    });

    describe('Allows alternate apiUser token: test_two', () => {
        var resp = false;
        var next = false;

        var req = {
            headers: {key: "token3"},
            url: '/test'
        };

        response.send = (request, message) => {
            resp = true;
        };
        token.verifyRequest(req, response, () => {
            next = true;
        });

        it('Did not send a Response', () => {
            assert.equal(resp, false);
        });

        it('Did run next', () => {
            assert.equal(next, true);
        });
    });

    describe('Denies alternate apiUser token: test_two, based on ruleset', () => {
        var resp = false;
        var next = false;

        var req = {
            headers: {key: "token3"},
            url: '/test/'
        };

        response.send = (request, message) => {
            resp = true;
        };
        token.verifyRequest(req, response, () => {
            next = true;
        });

        it('Did send a Response', () => {
            assert.equal(resp, true);
        });

        it('Did not run next', () => {
            assert.equal(next, false);
        });
    });
});