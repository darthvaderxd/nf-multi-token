'use strict';

const config = require('config-node')();
const HTTPStatus = require('http-status');

var ignored_routes = [];

/**
 * Add a route to allow without a token
 * @param string route - the route ie /hello/:world/
 */
var addIgnoreRoute = (route) => {
    ignored_routes.push(route);
};

var getIgnoredRoutes = () => {
    return ignored_routes;
};

/**
 * Verify the request has proper authentication
 * @param object req the request object
 * @param object res the response object
 * @param Function next the next function to happen
 */
var verifyRequest = (req, res, next) => {
    var key = config.nfMultiToken.apiKey;

    if (checkIgnored(req.url)) {
        return next();
    }

    if (typeof req.headers[key] != 'undefined' && verifyApiUser(req.headers[key], req.url)) {
        return next();
    } else {
        handleResponse(req, res, next);
    }
};

/**
 * Verify that the token given exists and is approved for this route
 * @param string token - the token given
 * @param string url - the requested url
 */
var verifyApiUser = (token, url) => {
    var apiUser = config.nfMultiToken.apiUsers.filter((user) => {
        return user.apiToken === token;
    })[0];

    if (apiUser) {
        return verifyRouteForUser(apiUser, url);
    }

    return false;
};


/**
 * Verify that a given route is allowed for an apiUser
 * @param object apiUser - user we are trying to verify
 * @param string url - the url that is being requested
 * @return boolean
 */
var verifyRouteForUser = (apiUser, url) => {
    var found = false;
    if (apiUser.routes.length === 1 && apiUser.routes[0] === "*") {
        found = true;
    } else {
        for(var i = 0; i < apiUser.routes.length; i++) {
            var route = apiUser.routes[i];
            if (matchRuleShort(url, route)) {
                found = true;
                break;
            }
        }
    }

    return found;
}

var handleResponse = (req, res, next) => {
    var ip = getRemoteIP(req);
    var response = {
        'code': "UnauthorizedRequest",
        'result': [{ "error": "Request Denied" }],
    };

    res.statusCode = HTTPStatus.UNAUTHORIZED;
    res.send(HTTPStatus.UNAUTHORIZED, response);

    /* istanbul ignore if */
    if (!process.env.SILENT) {
        console.log("NilFactor Token => request to %s denied for %s", req.url, ip);
    }
}

/**
 * Get the remote ip address of the client
 * @param object request - the request object
 * @return string
 */
var getRemoteIP = (request) => {
    if (typeof request != 'undefined' && typeof request.connection != 'undefined') {
        var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
        return ip.replace("::ffff:", "");
    } else {
        return 'ip address n/a';
    }
};

/**
 * Should this request be allowed with or without a token
 * @param string uri - the request uri/url
 * @return boolean
 */
var checkIgnored = (uri) => {
    if (ignored_routes.length > 0) {
        for (var i = 0; i < ignored_routes.length; i++) {
            var result = matchRuleShort(uri, ignored_routes[i])
            if (result) {
                return true;
            }
        }
    }

    return false;
};

var matchRuleShort = (str, rule) => {
  return new RegExp("^" + rule.split("*").join(".*") + "$").test(str);
};

module.exports.addIgnoreRoute = addIgnoreRoute;
module.exports.getIgnoredRoutes = getIgnoredRoutes;
module.exports.verifyRequest = verifyRequest;