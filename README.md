# nf-multi-token

NilFactor MultiToken is a simple package to allow you to add an api tokens to your restify or express server, which allow you to have multiple
api users with the ability to restrict requests to certain routes for that user.

```
//...
var token = require('nf-multi-token');
server.pre(token.verifyRequest);
//...
```

Create a config directory in your project with a json file called development.json or <NODE_ENV>.json whatever your NODE_ENV=thisname with the following, alter as you see fit, you can one user or many users.

```
{
    "nfMultiToken": {
        "apiKey": "key",
        "apiUsers": [
            {
                "apiUser": "master",
                "apiToken": "token1",
                "routes": ["*"]
            },
            {
                "apiUser": "test_one",
                "apiToken": "token2",
                "routes": ["/test/*"]
            },
            {
                "apiUser": "test_two",
                "apiToken": "token3",
                "routes": ["/test"]
            }
        ]
    }
}
```

If you want an api user to have access to all routes you would simply give the a routes object to match below

```
    {
        "apiUser": "master",
        "apiToken": "token1",
        "routes": ["*"]
    },
```

Then when you make requests to your api create a header key/pair value of "key: token1" (req.headers.key = "token1"), if using the above example.

To allow routes that do not require any api token simply do

```
token.addIgnoreRoute('/');
token.addIgnoreRoute('/hello/*');
```
